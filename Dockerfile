FROM docker.io/library/ubuntu:22.04

RUN mkdir /app

WORKDIR /app

COPY src/msgsend.sh /app/
COPY src/filedropper.sh /app/

RUN apt-get -y update &&\
    apt-get -y install curl jq gettext-base python3 python3-pip python3-venv &&\
    curl -L https://github.com/mtorromeo/mattersend/archive/2.0.tar.gz | tar -zxf - &&\
    cd mattersend-2.0 &&\
    python3 -m venv venv &&\
    . venv/bin/activate &&\
    python3 -m pip install --upgrade setuptools &&\
    python3 setup.py install &&\
    apt-get -y clean &&\
    apt-get -y autoremove


CMD [ "/app/msgsend.sh" ]
