#!/bin/bash
set -eEu -o pipefail


# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


secrets="$1"
msg="$2"



## create a copy with a nice name and the suffix (controls content type)
file=jobtechlinks_report_$(date -Iminutes | sed 's|[^[:alnum:]]|_|g').txt
cat - > "$file"


if ! . "$secrets" ; then echo "**** cannot source secrets $secrets" >&2; exit 1; fi
if [ ! -f "$file" ]; then echo "**** cannot find file $file" >&2; exit 1; fi


SIZE=$(wc -c "$file" | cut -f1 -d' ')


SESSFILES=$(mktemp)
trap "rm -f $SESSFILES.new $SESSFILES.ul $SESSFILES.msg" EXIT


## create upload session
curl "$SERVER"'/api/v4/uploads' \
-H 'Authorization: Bearer '"$BOTAUTH" \
-H 'Content-Type: application/json' \
--data-binary '{"channel_id": "'"$CHANID"'", "filename": "'"$file"'", "file_size": '"$SIZE"'}' > "$SESSFILES".new


upload_id=$(jq -r .id < "$SESSFILES".new)


## upload file to session
curl -X POST "$SERVER"'/api/v4/uploads/'"$upload_id" \
    -H 'Authorization: Bearer '"$BOTAUTH" --data-binary @"$file" > "$SESSFILES".ul


file_id=$(jq -r .id < "$SESSFILES".ul)


## send message with attached file
curl "$SERVER"'/api/v4/posts' \
-H 'Authorization: Bearer '"$BOTAUTH" \
-H 'Content-Type: application/json' \
--data-binary '{"channel_id": "'"$CHANID"'", "message": "'"$msg"'", "file_ids": ["'"$file_id"'"] }' > "$SESSFILES".msg
