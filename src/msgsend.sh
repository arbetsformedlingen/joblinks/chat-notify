#!/bin/bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "$self_dir"/mattersend-2.0/venv/bin/activate

secrets="${1:-}" ; shift
conf="${1:-}" ; shift
# The remaining args are report files to include


if ! . "$secrets" ; then echo "**** cannot find $secrets" >&2; exit 1; fi
if [ ! -f "$conf" ] ; then echo "**** cannot find $conf" >&2; exit 1; fi

mattersend --config "$conf" $@
